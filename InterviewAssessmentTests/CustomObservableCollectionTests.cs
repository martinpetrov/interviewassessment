﻿using System.Collections.Generic;
using DomainModelEditor.WpfBasis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InterviewAssessmentTests
{
    public class TestViewModel : ObservableObject
    {
        public TestViewModel(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }

        public override void Dispose()
        {
        }
    }

    [TestClass()]
    public class CustomObservableCollectionTests
    {
        [TestMethod()]
        public void LoadTest()
        {
            var collection = new CustomObservableCollection<TestViewModel>();

            var testData = new List<TestViewModel> { new TestViewModel("Person"), new TestViewModel("Employee") };
            collection.ReloadCollection(testData);

            Assert.AreEqual(2, collection.Count);
            Assert.AreEqual("Person", collection[0].Name);
            Assert.AreEqual("Employee", collection[1].Name);
        }

        [TestMethod()]
        public void GenericLoadTest()
        {
            var collection = new CustomObservableCollection<TestViewModel>();

            var testData = new List<string> { "Person", "Employee" };
            collection.ReloadCollection<string>(testData, i => new TestViewModel(i));

            Assert.AreEqual(2, collection.Count);
            Assert.AreEqual("Person", collection[0].Name);
            Assert.AreEqual("Employee", collection[1].Name);
        }

        [TestMethod()]
        public void AddTest()
        {
            var collection = new CustomObservableCollection<TestViewModel>();

            collection.Add(new TestViewModel("Shape"));

            Assert.AreEqual(1, collection.Count);
            Assert.AreEqual("Shape", collection[0].Name);
        }
    }
}