﻿using System;
using System.Diagnostics;
using System.Windows.Input;

namespace DomainModelEditor.WpfBasis
{
    // <summary>
    //// A command whose sole purpose is to relay its functionality to other
    //// objects by invoking delegates. The default return value for the
    //// CanExecute method is 'true'.
    //// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        private readonly Predicate<object> canExecute;

        private readonly Action<object> execute;
        private ICommand formBillsAndContinue;

        #endregion Fields

        #region Constructors

        //// <summary>
        //// Creates a new command that can always execute.
        //// </summary>
        //// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        public RelayCommand(ICommand formBillsAndContinue)
        {
            this.formBillsAndContinue = formBillsAndContinue;
        }

        //// <summary>
        //// Creates a new command.
        //// </summary>
        //// <param name="execute">The execution logic.</param>
        //// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }

            this.execute = execute;
            this.canExecute = canExecute;
        }

        #endregion Constructors

        #region ICommand Members

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return this.canExecute == null ? true : this.canExecute(parameters);
        }

        public void Execute(object parameters)
        {
            this.execute(parameters);
        }

        #endregion ICommand Members
    }
}
