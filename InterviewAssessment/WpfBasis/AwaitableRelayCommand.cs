﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DomainModelEditor.WpfBasis.Contracts;

namespace DomainModelEditor.WpfBasis
{
    public class AwaitableRelayCommand : IAsyncCommand, ICommand
    {
        private readonly Func<object, Task> executeMethod;
        private readonly RelayCommand underlyingCommand;
        private bool isExecuting;

        public AwaitableRelayCommand(Func<object, Task> executeMethod)
            : this(executeMethod, _ => true)
        {
        }

        public AwaitableRelayCommand(Func<object, Task> executeMethod, Predicate<object> canExecuteMethod)
        {
            this.executeMethod = executeMethod;
            this.underlyingCommand = new RelayCommand(x => { }, canExecuteMethod);
        }

        public event EventHandler CanExecuteChanged
        {
            add { this.underlyingCommand.CanExecuteChanged += value; }
            remove { this.underlyingCommand.CanExecuteChanged -= value; }
        }

        public ICommand Command
        {
            get
            {
                return this;
            }
        }

        public async Task ExecuteAsync(object args)
        {
            try
            {
                this.isExecuting = true;
                CommandManager.InvalidateRequerySuggested();
                await this.executeMethod(args);
            }
            finally
            {
                this.isExecuting = false;
                CommandManager.InvalidateRequerySuggested();
            }
        }

        public bool CanExecute(object parameter)
        {
            return !this.isExecuting && this.underlyingCommand.CanExecute(parameter);
        }

        public async void Execute(object parameter)
        {
            await this.ExecuteAsync(parameter);
        }
    }
}
