﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace DomainModelEditor.WpfBasis.Contracts
{
    public interface IAsyncCommand
    {
        ICommand Command { get; }

        Task ExecuteAsync(object args);

        bool CanExecute(object args);
    }
}
