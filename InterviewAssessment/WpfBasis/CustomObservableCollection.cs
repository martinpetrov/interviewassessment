﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DomainModelEditor.WpfBasis
{
    public class CustomObservableCollection<T> : ObservableCollection<T> where T : ObservableObject, IDisposable
    {
        public void ReloadCollection(IEnumerable<T> newItems)
        {
            if (newItems == null)
            {
                throw new ArgumentNullException(nameof(newItems));
            }

            this.ClearWithItemDispose();

            foreach (var item in newItems)
            {
                this.Add(item);
            }
        }

        public void ReloadCollection<G>(IEnumerable<G> newItems, Func<G, T> transform)
        {
            if (newItems == null)
            {
                throw new ArgumentNullException(nameof(newItems));
            }

            this.ClearWithItemDispose();

            foreach (var item in newItems)
            {
                this.Add(transform.Invoke(item));
            }
        }

        public void ClearWithItemDispose()
        {
            foreach (var item in this.Items)
            {
                item.Dispose();
            }

            this.Clear();
        }

        public void Dispose()
        {
            this.ClearWithItemDispose();
        }
    }
}
