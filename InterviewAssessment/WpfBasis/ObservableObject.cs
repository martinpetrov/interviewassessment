﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace DomainModelEditor.WpfBasis
{
    public abstract class ObservableObject : INotifyPropertyChanged, IDisposable
    {
        //// <summary>
        //// Raised when a property on this object has a new value.
        //// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        //// <summary>
        //// Returns whether an exception is thrown, or if a Debug.Fail() is used
        //// when an invalid property name is passed to the VerifyPropertyName method.
        //// The default value is false, but subclasses used by unit tests might
        //// override this property's getter to return true.
        //// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        //// <summary>
        //// Warns the developer if this object does not have
        //// a public property with the specified name. This
        //// method does not exist in a Release build.
        //// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public virtual void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                {
                    throw new Exception(msg);
                }

                Debug.Fail(msg);
            }
        }

        //// <summary>
        //// Raises this object's PropertyChanged event.
        //// </summary>
        //// <param name="propertyName">The property that has a new value.</param>
        public void NotifyPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            this.VerifyPropertyName(propertyName);

            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        public abstract void Dispose();

        //// <summary>
        //// Raises this object's PropertyChanged event.
        //// </summary>
        //// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var handler = this.PropertyChanged;
            this.VerifyPropertyName(propertyName);

            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }

        protected virtual void OnPropertiesChanged(params string[] properties)
        {
            foreach (var prop in properties)
            {
                this.OnPropertyChanged(prop);
            }
        }

        //// <summary>
        //// Resets the collection.
        //// </summary>
        //// <typeparam name="T"></typeparam>
        //// <param name="collection">The collection.</param>
        //// <returns>Empty ObservableCollection</returns>
        protected virtual ObservableCollection<T> ResetCollection<T>(ObservableCollection<T> collection)
        {
            if (collection == null)
            {
                collection = new ObservableCollection<T>();
            }

            collection.Clear();
            return collection;
        }
    }
}
