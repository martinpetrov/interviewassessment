﻿using System.Configuration;
using System.Threading.Tasks;
using System.Windows;
using DomainModelEditor.Database;
using DomainModelEditor.Logger;
using Serilog;

namespace interview_assessment
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private const string dbConnectionStringKey = "dbConnectionString";
        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            new LoggerConfiguration()
                 .ReadFrom.AppSettings()
                 .CreateLogger();

            string connectionString = ConfigurationManager.AppSettings[dbConnectionStringKey];

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                LoggerProvider.Logger.Error("Please set dbConnectionString in configuration!");
            }

            await Task.Run(async () => await DbProvider.IntiDbManagerAsync(new EntitySqliteManagerFactory(connectionString)));
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            DbProvider.CloseDbManager();
        }
    }
}
