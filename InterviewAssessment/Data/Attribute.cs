﻿namespace DomainModelEditor.Data
{
    public class Attribute
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int EntityId { get; set; }
    }
}
