﻿namespace DomainModelEditor.Data
{
    public class Entity
    {
        public Entity() { }

        public Entity(int id, string name, int xAxisPoint, int yAxisPoint)
        {
            Id = id;
            Name = name;
            XAxisPoint = xAxisPoint;
            YAxisPoint = yAxisPoint;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public double XAxisPoint { get; set; }

        public double YAxisPoint { get; set; }
    }
}
