﻿namespace DomainModelEditor.Database.Contracts
{
    public interface IEntityManagerFactory
    {
        IEntityDbManager Create();
    }
}