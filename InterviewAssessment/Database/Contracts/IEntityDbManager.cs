﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DomainModelEditor.Data;

namespace DomainModelEditor.Database.Contracts
{
    public interface IEntityDbManager : IDisposable
    {
        Task<bool> InitDbAsync();

        Task<bool> AddEntityAsync(Entity entity);

        Task<IEnumerable<Entity>> LoadAllEntitiesAsync();

        Task<bool> IsEntityNameExistsAsync(string entityName);

        Task<bool> UpdateEntityCoordinatesAsync(int entityId, double xAxisPoint, double yAxisPoint);

        Task<bool> AddAttributeAsync(Data.Attribute attribute);

        Task<bool> DeleteAttributeAsync(int attributeId);

        Task<IEnumerable<Data.Attribute>> LoadEntityAttributesAsync(int entityId);

        Task<bool> IsAttributeNameExistsAsync(string attributeName, int entityId);
    }
}
