﻿using DomainModelEditor.Database.Contracts;

namespace DomainModelEditor.Database
{
    public class EntitySqliteManagerFactory : IEntityManagerFactory
    {
        private readonly string dbConnection;

        public EntitySqliteManagerFactory(string dbConnectionString)
        {
            this.dbConnection = dbConnectionString;
        }

        public IEntityDbManager Create()
        {
            return new EntitySqliteManager(dbConnection);
        }
    }
}
