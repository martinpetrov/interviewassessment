﻿using System;
using System.Threading.Tasks;
using DomainModelEditor.Database.Contracts;

namespace DomainModelEditor.Database
{
    /// <summary>
    /// Use this static class to access singleton database connection.
    /// You have to use IntiDbManager method first to instantiate DbManager.
    /// When you do not need DbManager instance anymore you have to use CloseDbManager method to dispose all used resources.
    /// Use DbManager property to access db methods.
    /// </summary>
    public static class DbProvider
    {
        private static IEntityDbManager entityDbManager;

        public static async Task IntiDbManagerAsync(IEntityManagerFactory entityManagerFactory)
        {
            if (entityManagerFactory == null)
            {
                throw new ArgumentNullException(nameof(entityManagerFactory));
            }

            DbProvider.entityDbManager = entityManagerFactory.Create();
            await DbProvider.entityDbManager.InitDbAsync();
        }

        /// <summary>
        /// Close and dispose db connection.
        /// </summary>
        public static void CloseDbManager()
        {
            if (DbProvider.entityDbManager != null)
            {
                DbProvider.entityDbManager.Dispose();
                DbProvider.entityDbManager = null;
            }
        }

        /// <summary>
        /// Property to access db methods.
        /// </summary>
        public static IEntityDbManager DbManager
        {
            get
            {
                return DbProvider.entityDbManager;
            }
        }
    }
}
