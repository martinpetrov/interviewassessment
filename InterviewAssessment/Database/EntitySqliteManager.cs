﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainModelEditor.Data;
using DomainModelEditor.Database.Contracts;
using DomainModelEditor.Logger;
using SQLite;

namespace DomainModelEditor.Database
{
    public class EntitySqliteManager : IEntityDbManager
    {
        private readonly SQLiteAsyncConnection connection;

        private bool disposed = false;

        public EntitySqliteManager(string dbFileName)
        {
            this.connection = new SQLiteAsyncConnection(dbFileName);
        }

        ~EntitySqliteManager()
        {
            this.Dispose(false);
        }

        public async Task<bool> InitDbAsync()
        {
            try
            {
                    await this.connection.ExecuteAsync($@"
                        CREATE TABLE IF NOT EXISTS {CONSTANTS.ENTITIES_TABLE_NAME} (
                        {nameof(Entity.Id)} INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE, 
                        {nameof(Entity.Name)} VARCHAR  NOT NULL  UNIQUE)");

                    await this.connection.ExecuteAsync($@"
                        CREATE TABLE IF NOT EXISTS {CONSTANTS.COORDS_TABLE_NAME} (
                        {CONSTANTS.COORDS_TABLE_ID_COLUMN_NAME} INTEGER PRIMARY KEY  NOT NULL, 
                        {CONSTANTS.COORDS_TABLE_X_COLUMN_NAME} INTEGER NOT NULL,
                        {CONSTANTS.COORDS_TABLE_Y_COLUMN_NAME} INTEGER NOT NULL,
                        FOREIGN KEY ({CONSTANTS.COORDS_TABLE_ID_COLUMN_NAME}) REFERENCES {CONSTANTS.ENTITIES_TABLE_NAME} ({nameof(Entity.Id)}) ON DELETE NO ACTION ON UPDATE NO ACTION)");

                // Old entities table
                /*await this.connection.ExecuteAsync($@"
                        CREATE TABLE IF NOT EXISTS {CONSTANTS.ENTITIES_TABLE_NAME} (
                         {nameof(Entity.Id)} INTEGER PRIMARY KEY,
                         {nameof(Entity.Name)} TEXT UNIQUE NOT NULL,
                         {nameof(Entity.XAxisPoint)} REAL NOT NULL,
                         {nameof(Entity.YAxisPoint)} REAL NOT NULL
                        );");*/

                await this.connection.ExecuteAsync($@"
                            CREATE TABLE IF NOT EXISTS {CONSTANTS.ATTRIBUTES_TABLE_NAME} (
                             {nameof(Data.Attribute.Id)} INTEGER PRIMARY KEY,
                             {nameof(Data.Attribute.Name)} TEXT UNIQUE NOT NULL,
                             {nameof(Data.Attribute.EntityId)} INTEGER NOT NULL,
                            FOREIGN KEY ({nameof(Data.Attribute.EntityId)}) REFERENCES {CONSTANTS.ENTITIES_TABLE_NAME} ({nameof(Entity.Id)}) ON DELETE NO ACTION ON UPDATE NO ACTION
                            );");

                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }

        public async Task<bool> AddEntityAsync(Entity entity)
        {
            try
            {
                // Old table version select
                /*
                if (await this.connection.ExecuteAsync($"insert into {CONSTANTS.ENTITIES_TABLE_NAME} ({nameof(Entity.Name)}, {nameof(Entity.XAxisPoint)}, {nameof(Entity.YAxisPoint)}) values (?, ?, ?)",
                entity.Name,
                entity.XAxisPoint,
                entity.YAxisPoint) <= 0)*/

                this.connection.GetConnection().BeginTransaction();
                if (await this.connection.ExecuteAsync($"insert into {CONSTANTS.ENTITIES_TABLE_NAME} ({nameof(Entity.Name)}) values (?)",
                    entity.Name) <= 0)
                {
                    this.connection.GetConnection().Rollback();
                    LoggerProvider.Logger.Error($"Entity {entity.Name} is not added! Entity insert failed!");
                    return false;
                }

                int? lastInsertedEntityId = (await this.connection.QueryScalarsAsync<int>("SELECT last_insert_rowid()"))?.FirstOrDefault();

                if (!lastInsertedEntityId.HasValue ||
                    lastInsertedEntityId.Value <= 0)
                {
                    this.connection.GetConnection().Rollback();
                    LoggerProvider.Logger.Error($"Entity {entity.Name} is not added! Entity Id select failed!");
                    return false;
                }

                if (await this.connection.ExecuteAsync($"insert into {CONSTANTS.COORDS_TABLE_NAME} ({CONSTANTS.COORDS_TABLE_ID_COLUMN_NAME}, {CONSTANTS.COORDS_TABLE_X_COLUMN_NAME}, {CONSTANTS.COORDS_TABLE_Y_COLUMN_NAME}) values (?, ?, ?)",
                    lastInsertedEntityId.Value,
                    entity.XAxisPoint,
                    entity.YAxisPoint) <= 0)
                {
                    this.connection.GetConnection().Rollback();
                    LoggerProvider.Logger.Error($"Entity {entity.Name} is not added! Entity insert failed!");
                    return false;
                }

                this.connection.GetConnection().Commit();
                return true;
            }
            catch (Exception ex)
            {
                if (this.connection.GetConnection().IsInTransaction)
                {
                    this.connection.GetConnection().Rollback();
                }

                ex.LogError();
                return false;
            }
        }

        public async Task<bool> IsEntityNameExistsAsync(string entityName)
        {
            try
            {
                return ((await this.connection.QueryScalarsAsync<long>($"select exists(select 1 from {CONSTANTS.ENTITIES_TABLE_NAME}  where {nameof(Entity.Name)} like ?)", entityName))?.FirstOrDefault() ?? 0) > 0;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return true;
            }
        }

        public async Task<bool> UpdateEntityCoordinatesAsync(int entityId, double xAxisPoint, double yAxisPoint)
        {
            try
            {
                // Old db table update
                // if (await this.connection.ExecuteAsync($"update {CONSTANTS.ENTITIES_TABLE_NAME} set {nameof(Entity.XAxisPoint)} = ?, {nameof(Entity.YAxisPoint)} = ? where Id = ?",

                if (await this.connection.ExecuteAsync($"update {CONSTANTS.COORDS_TABLE_NAME} set {CONSTANTS.COORDS_TABLE_X_COLUMN_NAME} = ?, {CONSTANTS.COORDS_TABLE_Y_COLUMN_NAME} = ? where {CONSTANTS.COORDS_TABLE_ID_COLUMN_NAME} = ?",
                    xAxisPoint,
                    yAxisPoint,
                    entityId) <= 0)
                {
                    LoggerProvider.Logger.Error($"Entity with Id {entityId} is not updated!");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }

        public async Task<IEnumerable<Entity>> LoadAllEntitiesAsync()
        {
            try
            {
                // Old db table select
                // return await this.connection.QueryAsync<Entity>($"select * from {CONSTANTS.ENTITIES_TABLE_NAME}");

                return await this.connection.QueryAsync<Entity>($@"select e.{nameof(Entity.Id)}, e.{nameof(Entity.Name)}, 
                                                                    c.{CONSTANTS.COORDS_TABLE_X_COLUMN_NAME} as {nameof(Entity.XAxisPoint)},
                                                                    c.{CONSTANTS.COORDS_TABLE_Y_COLUMN_NAME} as {nameof(Entity.YAxisPoint)}
                                                                    from {CONSTANTS.ENTITIES_TABLE_NAME} as e
                                                                    join {CONSTANTS.COORDS_TABLE_NAME} as c on 
                                                                    e.{nameof(Entity.Id)} = c.{CONSTANTS.COORDS_TABLE_ID_COLUMN_NAME}");
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }

        public async Task<bool> IsAttributeNameExistsAsync(string attributeName, int entityId)
        {
            try
            {
                return ((await this.connection.QueryScalarsAsync<long>($"select exists(select 1 from {CONSTANTS.ATTRIBUTES_TABLE_NAME}  where {nameof(Data.Attribute.Name)} like ? and {nameof(Data.Attribute.EntityId)} = ?)", attributeName, entityId))?.FirstOrDefault() ?? 0) > 0;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return true;
            }
        }

        public async Task<bool> AddAttributeAsync(Data.Attribute attribute)
        {
            try
            {
                if (await this.connection.ExecuteAsync($"insert into {CONSTANTS.ATTRIBUTES_TABLE_NAME} ({nameof(Data.Attribute.Name)}, {nameof(Data.Attribute.EntityId)}) values (?, ?)",
                    attribute.Name,
                    attribute.EntityId) <= 0)
                {
                    LoggerProvider.Logger.Error($"Attribute {attribute.Name} is not added!");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ex.LogError();
                return false;
            }
        }

        public async Task<bool> DeleteAttributeAsync(int attributeId)
        {
            if (await this.connection.ExecuteAsync($"delete from {CONSTANTS.ATTRIBUTES_TABLE_NAME} where Id = ?",
                attributeId) <= 0)
            {
                LoggerProvider.Logger.Error($"Attribute with Id {attributeId} is not deleted!");
                return false;
            }

            return true;
        }

        public async Task<IEnumerable<Data.Attribute>> LoadEntityAttributesAsync(int entityId)
        {
            try
            {
                return await this.connection.QueryAsync<Data.Attribute>($"select * from {CONSTANTS.ATTRIBUTES_TABLE_NAME} where {nameof(Data.Attribute.EntityId)} = ?", entityId);
            }
            catch (Exception ex)
            {
                ex.LogError();
                return null;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            if (disposing)
            {
                // dispose managed objects here if any.
            }

            // free unmanaged unmanaged objects
            if (this.connection != null)
            {
                this.connection.GetConnection().Close();
                this.connection.GetConnection().Dispose();
            }

            this.disposed = true;
        }
    }
}
