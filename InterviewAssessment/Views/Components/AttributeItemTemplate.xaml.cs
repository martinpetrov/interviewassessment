﻿using System.Windows.Controls;

namespace DomainModelEditor.Views.Components
{
    /// <summary>
    /// Interaction logic for AttributeItemTemplate.xaml
    /// </summary>
    public partial class AttributeItemTemplate : Grid
    {
        public AttributeItemTemplate()
        {
            this.InitializeComponent();
        }
    }
}
