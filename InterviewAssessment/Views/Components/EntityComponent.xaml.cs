﻿using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace DomainModelEditor.Views.Components
{
    /// <summary>
    /// Interaction logic for EntityComponent.xaml
    /// </summary>
    public partial class EntityComponent : Grid
    {
        public EntityComponent()
        {
            this.InitializeComponent();
        }

        private void UserControl_MouseEnter(object sender, MouseEventArgs e)
        {
            this.ItemRectangle.Fill = Brushes.LightGreen;
            this.Cursor = Cursors.Hand;
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {
            this.ItemRectangle.Fill = Brushes.LightBlue;
            this.Cursor = Cursors.Arrow;
        }

        private void UserControl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.ItemRectangle.Fill = Brushes.Green;
        }

        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.ItemRectangle.Fill = Brushes.LightGreen;
        }
    }
}
