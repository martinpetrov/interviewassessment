﻿using System;
using Serilog;

namespace DomainModelEditor.Logger
{
    public static class LoggerProvider
    {
        private static ILogger logger;

        static LoggerProvider()
        {
            LoggerProvider.logger = new LoggerConfiguration()
                 .ReadFrom.AppSettings()
                 .CreateLogger();
        }

        public static ILogger Logger
        {
            get
            {
                return LoggerProvider.logger;
            }
        }

        public static void LogError(this Exception ex)
        {
            LoggerProvider.logger.Error($"{ex.Message} {ex.InnerException} {ex.StackTrace}");
        }
    }
}
