﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;

namespace DomainModelEditor.Behaviors
{
    /// <summary>
    /// This Behavior adds drag&drop functionality to any usercontrol inside Canvas panel.
    /// !!! Please be aware when use this behavior all mouse events are suppressed !!!
    /// </summary>
    public class DragBehavior : Behavior<FrameworkElement>, INotifyPropertyChanged
    {
        public readonly TranslateTransform transform = new TranslateTransform();
        private Point panelMouseStartPosition;
        private Point itemStartPosition;

        public static bool GetParentPanel(DependencyObject obj)
        {
            return (bool)obj.GetValue(ParentPanelProperty);
        }

        public static void SetParentPanel(DependencyObject obj, bool value)
        {
            obj.SetValue(ParentPanelProperty, value);
        }

        public static readonly DependencyProperty ParentPanelProperty =
          DependencyProperty.RegisterAttached("ParentPanel",
          typeof(Canvas), typeof(DragBehavior),
          null);

        public static readonly DependencyProperty DropCommandProperty = 
            DependencyProperty.Register("DropCommand", 
                typeof(ICommand), typeof(DragBehavior), 
                null);

        public static readonly DependencyProperty DropCommandParameterProperty = 
            DependencyProperty.Register("DropCommandParameter", 
                typeof(object), typeof(DragBehavior),
                null);

        public event PropertyChangedEventHandler PropertyChanged;

        public Canvas ParentPanel
        {
            get { return (Canvas)GetValue(ParentPanelProperty); }
            set { SetValue(ParentPanelProperty, value); }
        }

        public Point CurrentItemCoordinates
        {
            get
            {
                if (this.ParentPanel != null)
                {
                    return this.AssociatedObject.TransformToAncestor(this.ParentPanel).Transform(new Point(0, 0));
                }

                return new Point(0, 0);
            }
        }
        
        public ICommand DropCommand
        {
            get { return (ICommand)GetValue(DropCommandProperty); }
            set { SetValue(DropCommandProperty, value); }
        }

        public object DropCommandParameter
        {
            get { return (object)GetValue(DropCommandParameterProperty); }
            set { SetValue(DropCommandParameterProperty, value); }
        }

        protected override void OnAttached()
        {
            this.AssociatedObject.RenderTransform = this.transform;
            this.AssociatedObject.MouseLeftButtonDown += this.ElementOnMouseLeftButtonDown;
            this.AssociatedObject.PreviewMouseLeftButtonUp += this.ElementOnMouseLeftButtonUp;
            this.AssociatedObject.MouseMove += this.ElementOnMouseMove;
        }

        private void ElementOnMouseLeftButtonDown(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            if (this.ParentPanel == null)
            {
                return;
            }

            this.itemStartPosition = this.AssociatedObject.TransformToAncestor(this.ParentPanel).Transform(new Point(0, 0));
            this.panelMouseStartPosition = mouseButtonEventArgs.GetPosition(this.ParentPanel);
            ((UIElement)sender).CaptureMouse();
        }

        private void ElementOnMouseLeftButtonUp(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            if (((UIElement)sender).IsMouseCaptured)
            {
                ((UIElement)sender).ReleaseMouseCapture();

                this.AssociatedObject.RenderTransformOrigin = this.CurrentItemCoordinates;
                this.transform.X = 0;
                this.transform.Y = 0;

                if (this.DropCommand != null &&
                    this.DropCommand.CanExecute(this.DropCommandParameter))
                {
                    this.DropCommand.Execute(this.DropCommandParameter);
                }
            }
        }

        private void ElementOnMouseMove(object sender, MouseEventArgs mouseButtonEventArgs)
        {
            if (this.ParentPanel == null)
            {
                return;
            }

            var newPanelMousePos = mouseButtonEventArgs.GetPosition(this.ParentPanel);
            var diff = (newPanelMousePos - this.panelMouseStartPosition);

            if (!((FrameworkElement)sender).IsMouseCaptured) 
            {
                return;
            }

            if (this.itemStartPosition.X + diff.X < 0)
            {
                return;
            } 

            if (this.itemStartPosition.Y + diff.Y < 0)
            {
                return;
            }

            if (this.itemStartPosition.X + this.AssociatedObject.ActualWidth + diff.X > this.ParentPanel.ActualWidth)
            {
                return;
            }

            if (this.itemStartPosition.Y + this.AssociatedObject.ActualHeight + diff.Y > this.ParentPanel.ActualHeight)
            {
                return;
            }

            this.transform.X = diff.X;
            this.transform.Y = diff.Y;
            
            this.OnPropertyChanged(nameof(this.CurrentItemCoordinates));
        }

        //// <summary>
        //// Raises this object's PropertyChanged event.
        //// </summary>
        //// <param name="propertyName">The property that has a new value.</param>
        protected virtual void OnPropertyChanged(string propertyName = "")
        {
            var handler = this.PropertyChanged;

            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
        }
    }
}
