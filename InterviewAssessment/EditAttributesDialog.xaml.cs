﻿using System.Windows;

namespace DomainModelEditor
{
    /// <summary>
    /// Interaction logic for AddAttributesDialog.xaml
    /// </summary>
    public partial class EditAttributesDialog : Window
    {
        public EditAttributesDialog()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
