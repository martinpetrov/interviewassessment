﻿namespace DomainModelEditor
{
    public static class CONSTANTS
    {
        public const string ENTITIES_TABLE_NAME = "entities";

        public const string ATTRIBUTES_TABLE_NAME = "attributes";

        public const string COORDS_TABLE_NAME = "coords";

        public const string COORDS_TABLE_ID_COLUMN_NAME = "Id";

        public const string COORDS_TABLE_X_COLUMN_NAME = "x";

        public const string COORDS_TABLE_Y_COLUMN_NAME = "y";
    }
}
