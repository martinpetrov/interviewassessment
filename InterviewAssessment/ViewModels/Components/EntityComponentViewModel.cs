﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DomainModelEditor.Data;
using DomainModelEditor.Database;
using DomainModelEditor.WpfBasis;

namespace DomainModelEditor.ViewModels.Components
{
    public class EntityComponentViewModel : ObservableObject
    {
        private readonly Entity entityModel;
        private ICommand entityMovedCommand;
        private ICommand entityOpenAttributesCommand;
        private ICommand entityComponentLoadedCommand;

        private bool isLoaded = false;

        public EntityComponentViewModel(Entity entityModel)
        {
            this.entityModel = entityModel;
            this.Attributes = new ObservableCollection<string>();
        }

        public Entity EntityModel
        {
            get
            {
                return this.entityModel;
            }
        }

        public string Name
        {
            get
            {
                return this.EntityModel?.Name;
            }
        }

        public double XAxisPoint
        {
            get
            {
                return this.EntityModel?.XAxisPoint ?? 0;
            }
            set
            {
                this.EntityModel.XAxisPoint = value;
                this.OnPropertyChanged();
            }
        }

        public double YAxisPoint
        {
            get
            {
                return this.EntityModel?.YAxisPoint ?? 0;
            }
            set
            {
                this.EntityModel.YAxisPoint = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand EntityComponentLoadedCommand
        {
            get
            {
                if (this.entityComponentLoadedCommand == null)
                {
                    this.entityComponentLoadedCommand = new AwaitableRelayCommand(this.EntityComponentLoadedAsync);
                }

                return this.entityComponentLoadedCommand;
            }
        }

        public ICommand EntityMovedCommand
        {
            get
            {
                if (this.entityMovedCommand == null)
                {
                    this.entityMovedCommand = new AwaitableRelayCommand(this.EntityMovedAsync);
                }

                return this.entityMovedCommand;
            }
        }

        public ICommand EntityOpenAttributesCommand
        {
            get
            {
                if (this.entityOpenAttributesCommand == null)
                {
                    this.entityOpenAttributesCommand = new AwaitableRelayCommand(this.EntityOpenAttributesAsync);
                }

                return this.entityOpenAttributesCommand;
            }
        }

        public ObservableCollection<string> Attributes { get; set; }

        private async Task EntityMovedAsync(object args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            if (!(args is Point))
            {
                throw new ArgumentException($"nameof(args) can be only type of Point!");
            }

            Point newPoint = (Point) args;

            this.XAxisPoint = newPoint.X;
            this.YAxisPoint = newPoint.Y;
            if (!await DbProvider.DbManager.UpdateEntityCoordinatesAsync(this.EntityModel.Id, newPoint.X, newPoint.Y))
            {
                MessageBox.Show($"Error while coordinates updating!{Environment.NewLine}Please see log files for details!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task EntityOpenAttributesAsync(object args)
        {
            using (EditAttributesViewModel editAttributesVm = new EditAttributesViewModel(this.entityModel.Id))
            {
                EditAttributesDialog editAttributesDialog = new EditAttributesDialog();
                editAttributesDialog.DataContext = editAttributesVm;
                editAttributesDialog.ShowDialog();
                await this.ReloadAttributesAsync();
            }
        }

        private async Task EntityComponentLoadedAsync(object args)
        {
            if (!this.isLoaded)
            {
                this.isLoaded = true;

                await this.ReloadAttributesAsync();
            }
        }

        private async Task ReloadAttributesAsync()
        {
            this.Attributes.Clear();

            var attributes = await DbProvider.DbManager.LoadEntityAttributesAsync(this.entityModel.Id);

            if (attributes != null)
            {
                foreach (var attribute in attributes)
                {
                    this.Attributes.Add(attribute.Name);
                }
            }
        }

        public override void Dispose()
        {
            if (this.Attributes != null)
            {
                this.Attributes.Clear();
                this.Attributes = null;
            }
        }
    }
}
