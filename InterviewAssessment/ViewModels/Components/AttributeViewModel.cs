﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DomainModelEditor.Database;
using DomainModelEditor.WpfBasis;

namespace DomainModelEditor.ViewModels.Components
{
    public class AttributeViewModel : ObservableObject
    {
        private readonly Data.Attribute attribute;

        private ICommand deleteAttributeCommand;

        public AttributeViewModel(Data.Attribute attribute)
        {
            if (attribute == null)
            {
                throw new ArgumentNullException(nameof(attribute));
            }

            this.attribute = attribute;
        }

        public event EventHandler<EventArgs> AttributeDeleted;

        public ICommand DeleteAttributeCommand
        {
            get
            {
                if (this.deleteAttributeCommand == null)
                {
                    this.deleteAttributeCommand = new AwaitableRelayCommand(this.DeleteAttributeAsync);
                }

                return this.deleteAttributeCommand;
            }
        }

        public string AttributeName
        {
            get
            {
                return this.attribute.Name;
            }
        }

        public override void Dispose()
        {
        }

        private async Task DeleteAttributeAsync(object args)
        {
            if(!await DbProvider.DbManager.DeleteAttributeAsync(this.attribute.Id))
            {
                MessageBox.Show($"Error while attribute deleting.{Environment.NewLine}Please check logs for details!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            this.AttributeDeleted?.Invoke(this, null);
        }
    }
}
