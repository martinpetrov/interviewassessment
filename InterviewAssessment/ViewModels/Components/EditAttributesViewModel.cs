﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DomainModelEditor.Database;
using DomainModelEditor.WpfBasis;

namespace DomainModelEditor.ViewModels.Components
{
    public class EditAttributesViewModel : ObservableObject
    {
        private readonly int entityId;

        private ICommand editAttributesUCLoadedCommand;
        private ICommand addAttributeCommand;

        private string newAttributeName;
        private bool isLoaded = false;

        public EditAttributesViewModel(int entityId)
        {
            if (entityId < 0)
            {
                throw new ArgumentOutOfRangeException($"{nameof(entityId)} cannot be less than 0!");
            }

            this.entityId = entityId;
            this.Attributes = new CustomObservableCollection<AttributeViewModel>();
        }

        public ICommand EditAttributesUCLoadedCommand
        {
            get
            {
                if (this.editAttributesUCLoadedCommand == null)
                {
                    this.editAttributesUCLoadedCommand = new AwaitableRelayCommand(this.EditAtributesUCLoadedAsync);
                }

                return this.editAttributesUCLoadedCommand;
            }
        }

        public ICommand AddAttributeCommand
        {
            get
            {
                if (this.addAttributeCommand == null)
                {
                    this.addAttributeCommand = new AwaitableRelayCommand(this.AddAttributeAsync, this.CanAddAttributeAsync);
                }

                return this.addAttributeCommand;
            }
        }

        public string NewAttributeName
        {
            get
            {
                return this.newAttributeName;
            }

            set
            {
                this.newAttributeName = value;
                this.OnPropertyChanged();
            }
        }

        public CustomObservableCollection<AttributeViewModel> Attributes { get; set; }

        public override void Dispose()
        {
            if (this.Attributes != null)
            {
                this.DettachAtributesEvents();
                this.Attributes.ClearWithItemDispose();
                this.Attributes = null;
            }
        }

        private async Task AddAttributeAsync(object args)
        {
            if (await DbProvider.DbManager.IsAttributeNameExistsAsync(this.NewAttributeName, this.entityId))
            {
                MessageBox.Show($"Attribute {this.NewAttributeName} is added already!", "Attribute exists.", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!await DbProvider.DbManager.AddAttributeAsync(new Data.Attribute()
            {
                EntityId = this.entityId,
                Name = this.NewAttributeName,
            }))
            {
                MessageBox.Show($"Error while attribute adding.{Environment.NewLine}Please check logs for details!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            this.NewAttributeName = string.Empty;
            await this.ReloadAttributesAsync();
        }

        private bool CanAddAttributeAsync(object args)
        {
            if (string.IsNullOrWhiteSpace(this.NewAttributeName))
            {
                return false;
            }

            return true;
        }

        private async Task EditAtributesUCLoadedAsync(object args)
        {
            if (!this.isLoaded)
            {
                this.isLoaded = true;

                await this.ReloadAttributesAsync();
            }
        }

        private async Task ReloadAttributesAsync()
        {
            this.DettachAtributesEvents();
            this.Attributes.ClearWithItemDispose();

            var attributes = await DbProvider.DbManager.LoadEntityAttributesAsync(this.entityId);

            if (attributes != null)
            {
                this.Attributes.ReloadCollection(attributes, i =>
                {
                    var attributeVM = new AttributeViewModel(i);
                    attributeVM.AttributeDeleted += AttributeVM_AttributeDeleted;
                    return attributeVM;
                });
            }
        }

        private async void AttributeVM_AttributeDeleted(object sender, System.EventArgs e)
        {
            await this.ReloadAttributesAsync();
        }

        private void DettachAtributesEvents()
        {
            if (this.Attributes != null)
            {
                foreach (var attribute in this.Attributes)
                {
                    attribute.AttributeDeleted -= AttributeVM_AttributeDeleted;
                }
            }
        }
    }
}
