﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DomainModelEditor.Database;
using DomainModelEditor.WpfBasis;

namespace DomainModelEditor.ViewModels
{
    public class AddEntityViewModel : ObservableObject
    {
        private ICommand addEntityCommand;
        private ICommand closeWindowCommand;

        public string EntityName { get; set; }

        public ICommand AddEntityCommand
        {
            get
            {
                if (this.addEntityCommand == null)
                {
                    this.addEntityCommand = new AwaitableRelayCommand(this.AddEntityAsync, this.CanAddEntity);
                }

                return this.addEntityCommand;
            }
        }

        public ICommand CloseWindowCommand
        {
            get
            {
                if (this.closeWindowCommand == null)
                {
                    this.closeWindowCommand = new RelayCommand(this.CloseWindow);
                }

                return this.closeWindowCommand;
            }
        }

        public override void Dispose()
        {
        }

        private async Task AddEntityAsync(object args)
        {
            if (args == null || !(args is Window))
            {
                return;
            }

            Window window = args as Window;

            if (await DbProvider.DbManager.IsEntityNameExistsAsync(this.EntityName))
            {
                MessageBox.Show($"Entity with name {this.EntityName} exists already!", $"{this.EntityName} exists.", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            window.DialogResult = true;
            window.Close();
        }

        private bool CanAddEntity(object args)
        {
            if (args == null || !(args is Window))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(this.EntityName))
            {
                return false;
            }

            return true;
        }

        private void CloseWindow(object args)
        {
            if (args == null || !(args is Window))
            {
                return;
            }

            Window window = args as Window;

            window.DialogResult = false;
            window.Close();
        }

    }
}
