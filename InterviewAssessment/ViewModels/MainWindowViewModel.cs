﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DomainModelEditor.Data;
using DomainModelEditor.Database;
using DomainModelEditor.ViewModels.Components;
using DomainModelEditor.WpfBasis;

namespace DomainModelEditor.ViewModels
{
    public class MainWindowViewModel : ObservableObject
    {
        private ICommand windowLoadedCommand;
        private ICommand addEntityCommand;

        private bool isLoaded = false;

        public MainWindowViewModel()
        {
            this.Entities = new CustomObservableCollection<EntityComponentViewModel>();
        }

        public ICommand WindowLoadedCommand
        {
            get
            {
                if (this.windowLoadedCommand == null)
                {
                    this.windowLoadedCommand = new AwaitableRelayCommand(this.WindowLoadedAsync);
                }

                return this.windowLoadedCommand;
            }
        }

        public ICommand AddEntityCommand
        {
            get
            {
                if (this.addEntityCommand == null)
                {
                    this.addEntityCommand = new AwaitableRelayCommand(this.AddEntityAsync);
                }

                return this.addEntityCommand;
            }
        }

        public CustomObservableCollection<EntityComponentViewModel> Entities { get; set; }

        private async Task WindowLoadedAsync(object args)
        {
            if (!this.isLoaded)
            {
                this.isLoaded = true;
                await this.ReloaldDataAsync();
            }
        }

        private async Task AddEntityAsync(object args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            var param = args as object[];

            if (param == null || param.Length < 2)
            {
                throw new ArgumentException("args has to be array with two items (container ActualWidth and ActualHeight)");
            }

            int actualWidth = (int)(double)param[0];
            int actualHeight = (int)(double)param[1];

            var popup = new AddEntityDialog();
            popup.ShowDialog();

            using (var dataContext = popup.DataContext as AddEntityViewModel)
            {
                if (popup.DialogResult.HasValue &&
                    popup.DialogResult.Value &&
                    dataContext != null &&
                    !string.IsNullOrWhiteSpace(dataContext.EntityName))
                {
                    var randomNrGenerator = new Random();
                    if (!await DbProvider.DbManager.AddEntityAsync(new Entity(0, dataContext.EntityName, randomNrGenerator.Next(80, actualWidth / 2), randomNrGenerator.Next(50, actualHeight / 2))))
                    {
                        MessageBox.Show($"Entity is not added.{Environment.NewLine} Please see log files for details!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }

                    await this.ReloaldDataAsync();
                }
            }
        }

        private async Task ReloaldDataAsync()
        {
            this.Entities.ClearWithItemDispose();

            var entities = await DbProvider.DbManager.LoadAllEntitiesAsync();

            if (entities != null)
            {
                this.Entities.ReloadCollection(entities, i => new EntityComponentViewModel(i));
            }
        }

        public override void Dispose()
        {
            if (this.Entities != null)
            {
                this.Entities.Dispose();
                this.Entities = null;
            }
        }
    }
}
