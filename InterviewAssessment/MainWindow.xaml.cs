﻿using System.Windows;
using DomainModelEditor.WpfBasis;

namespace DomainModelEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();

            Dispatcher.ShutdownStarted += Dispatcher_ShutdownStarted;
        }

        private void Dispatcher_ShutdownStarted(object sender, System.EventArgs e)
        {
            if (this.DataContext != null && this.DataContext is ObservableObject)
            {
                (this.DataContext as ObservableObject)?.Dispose();
            }
        }
    }
}
